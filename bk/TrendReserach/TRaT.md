# TRaT
<span style="color:red">T</span>rand <span style="color:red">R</span>search <span style="color:red">a</span>nd <span style="color:red">T</span>rial

---

## <span style="color:orange">**Reveal.js**</span>
#### というツールを使ってます

---

## 利点
#### 簡単にスタイリッシュなスライドが作れる
- markdownで書いた内容をhtmlに変換
- メモをそのままスライドに
- html, css記法で<span style="color:green; font-size:60px">工夫可能</span>
- テンプレートいろいろ (https://www.cyamax.com/entry/2018/01/12/002044)
- 画像も入れれる![logo](symbolmark.gif)
<aside class="notes">
  発表者が見るノート   
  伝えたいことをメモ  
</aside>

>>>

以下の 感じで書けばいい
- インライン使えるから、<br>
  コード載せるのにも便利

```
## 利点
### 簡単にスタイリッシュなスライドが
### 作れる
- markdownで書いた内容をhtmlに変換
- メモをそのままスライドに
- html, css記法で<span style="color:green">工夫可能</span>
- テンプレートいろいろ (https://www.cyamax.com/entry/2018/01/12/002044)
```

---

サイト<br>
https://revealjs.com/#/

使い方<br>
https://qiita.com/t-kusakabe/items/725e7438892bba395062
