# **テーマ紹介**
<br>

### <span style="color:teal">品川 勇太</span>

---

# <span style="color:tomato">DevOps</span>
### って知ってますか？

>>>

## <span style="color:tomato">DevOps</span>
#### = 「開発チーム（**Dev**elopment）と運用チーム（**Op**eration**s**）がお互いに協調し合うことで、そのビジネスの価値をより確実かつ迅速にエンドユーザーに届け続ける」という概念

>>>

## <span style="color:royalblue">今まで
- 開発と運用・保守は分離
- 開発が時間をかけて作ったものを、<br>運用・保守に引き渡して管理<br><br>


## <span style="color:orange">これから
- 開発のサイクルのスピードアップ
- 作ったものを次々にリリースするため、<br>開発と保守の協力が不可欠

>>>

## <span style="color:tomato">DevOps</span>
![](キャプチャ4.png)

>>>

### <span style="color:tomato">**DevOps**</span>の要素
- バージョン管理
- 構成管理
- CI
- ・・・
- <span style="font-size:150%; color:yellow">チャット</span>を使ったコミュニケーション
![](hqdefault.jpg)

---

### さらに、そこで注目されている概念が

---

# <span style="color:yellow">**ChatOps**

>>>

## <span style="color:yellow">ChatOps
#### = チャットにロボットを登場させて<br>人の作業を代替する

![](img2.png)

>>>

## 例えば
- ビルドの結果を確認
- リマインダー
- 環境情報を教えてもらう
- etc...

---

#### テーマ
### TRaTで使うslackにbotを登場させて
### プランニングポーカーを行う

>>>

### 構成
![](キャプチャ.png)

>>>

### or
![](キャプチャ3.png)

---

### トライアルの流れ
1. pythonとslackbotをインストール
1. 簡単なプログラムでslackとの疎通確認
1. プランニングポーカーのロジック作成

---

## 今後の展望
- Redmineのタスクのリマインダー
- 今後入れるツールとの連携
- 各々が自由にbotを作って登場させる

---

## テーマの押しポイント
- 実際に業務で活用できそう！
- 今後の活動で各々拡張して遊べる！
- 適度にロジック考えるプログラミングがあってモブプロに向きそう

---

### 参考
https://qiita.com/sukesuke/items/1ac92251def87357fdf6
https://qiita.com/kunitaya/items/690028e33ba5c666f3e2